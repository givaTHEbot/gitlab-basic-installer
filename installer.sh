#!/bin/bash

operating_sys=("Centos" "Redhat") #This script is for rpm based only as of now!

platform=$(lsb_release -is)
#platform=plat
packages=("yum" "sudo" "openssh" "firewalld" "curl" "policycoreutils-python" "openssh-server" "perl" "postfix" "gitlab-ee")
red='\033[0;31m'
NC='\033[0m' # No Color
green='\033[0;32m'
green_bg='\033[42m'
red_bg='\033[41m'
yellow_bg='\033[43m'
cyan_bg='\033[1;46m'
yellow='\033[0;33m'
cyan='\033[0;36m'

ERROR="${red_bg}ERROR${NC}"
WARN="${yellow}WARNING${NC}"
INFO="${cyan}INFO${NC}"

error_check(){
	if [[ $2 == 0 ]]
	then
		echo -e $1
	else
		echo -e $1
		exit $2
	fi
}

os_check(){
	echo ""
	[[ "$OSTYPE" == "linux-gnu"* ]] || error_check "${ERROR}: The OS type doesn't match!" 1
	shopt -s nocasematch
	if [[ "${operating_sys[@]} " =~ "${platform}" ]]
	then
		os_fine=0
		pkg_cmd="rpm"
	else
		os_fine=1
	fi
	shopt -u nocasematch
	echo -e "${INFO}: This is a $OSTYPE server and the platform is ${platform}."
	[[ $os_fine == 1 ]] && error_check "${ERROR}: This script is not supported in platform ${platform}. Exiting" 1
	echo ""
}

resource_check(){
	avail_in_slash=$(df -h | grep /$ | awk '{print $4}')
	avail_cpu=$(nproc --all)
	avail_mem=$(free -g | awk '/^Mem:/{print $2}')
	avail_swap=$(free -g | grep -i swap | awk '{print $2}')

	echo ""
	[[ $avail_cpu -le 3 ]] && { avail_cpu=${red_bg}${avail_cpu}${NC} ;echo -e "${WARN}: It is recommended to have 4 Cores, you server appears to have only $avail_cpu core/s"; }
	[[ $avail_mem -le 3 ]] && { avail_mem=${red_bg}${avail_mem}${NC} ;echo -e "${WARN}: It is recommended to have atleast 4GB RAM memory, your server appears to have only $avail_mem GB memory"; }
	[[ $avail_swap -le 3 ]] && { avail_swap=${red_bg}${avail_swap}${NC} ;echo -e "${WARN}: It is recommended to have atleast 4GB swap memory, your server appears to have only $avail_swap GB swap memory"; }
	[[ $load_avg -eq 1 ]] && echo -e "${WARN}: The load avaerage in the server is higher than usual. The current load average is mentioned below, please verify and continue.\n\n ${cyan_bg}$(w | grep load | awk -F',' '{print $(NF-2), $(NF-1), $NF}')${NC}"
	echo ""
	
	resource_op="============ | ============ | ============ \nResource | Recommended | Availability\n ============ | ============ | ============ \nDisk_Space | Read_Note_Below | $avail_in_slash\nCPU | 4_Cores | $avail_cpu \nMemory | 4GB | $avail_mem\nSwap_Space | 4GB | $avail_swap"
	echo -e $resource_op | column -t
	echo ""
	echo -e "${INFO}: Note - The necessary hard drive space largely depends on the size of the repositories you want to store in GitLab but as a rule of thumb you should have at least as much free space as all your repositories combined take up."
	echo ""
}

rpm_check(){
	pkg_output=" ======== | ======== \nRPM_Name | Status\n ======== | ======== "
	for package in ${packages[@]}
	do
		${pkg_cmd} -qa | grep ^$package 2>&1 > /dev/null; exit_stat="$?"
		if [[ ${exit_stat} -eq 0 ]]
		then
			pkg_output="${pkg_output}\n ${package} | ${green_bg}INSTALLED${NC}"
		else
			pkg_output="${pkg_output}\n ${package} | ${red_bg}NOT-INSTALLED${NC}"
		fi
	done
	echo -e $pkg_output | column -t
	echo ""; echo -e "${INFO}: The RPMs which are not installed will get installed once you continue."
	echo -e $pkg_output | column -t | grep NOT-INSTALLED | awk '{print $1}'
}


health_check(){
	load_1=$(w | grep load | awk '{print $(NF-1)}' | tr -d ,)
	load_2=$(w | grep load | awk '{print $(NF-2)}' | tr -d ,)
	if [[ $load_1 > 1.5 ]] || [[ $load_2 > 1.5 ]]
	then
		load_avg=1
	else
		load_avg=0
	fi
}


pre_checks(){
	echo ""
	rpm -qa | egrep -i 'gitlab-ce|gitlab-ee' 2>&1 > /dev/null
        [[ $? -eq 0 ]] && error_check "${INFO}: Congrats! ${red}Gitlab installation is already in this server.${NC} You are one step ahead of us!" 1
	os_check
	resource_check
	health_check
	rpm_check
	echo ""
	echo -e "${INFO}: The installation might fail if the recommendations are not satisfied. Please verify above logs."
	echo ""; read -p  "Do you want to continue (Y/N) ? " cont
}

main(){

echo -e "${INFO}: Starting the installation of packages.."
for package in $(echo -e $pkg_output | column -t | grep NOT-INSTALLED | grep -v gitlab-ee | awk '{print $1}')
do
	echo -e "${INFO}: Installation of ${package} started."
	sudo yum install -y ${package}
	echo -e "${INFO}: Installation of ${package} completed."
done

echo ""
echo -e "${INFO}: Enabling services.."
sudo systemctl enable sshd || error_check "${ERROR}: sshd enabling failed, fix it and try again." 1
sudo systemctl start sshd || error_check "${ERROR}: sshd starting failed, fix it and try again." 1
sudo firewall-cmd --permanent --add-service=http || error_check "${ERROR}: firewalld http add-service failed, fix it and try again." 1
sudo firewall-cmd --permanent --add-service=https || error_check "${ERROR}: firewalld http add-service failed, fix it and try again." 1
sudo systemctl reload firewalld || error_check "${ERROR}: firewalld reloading failed, fix it and try again." 1
sudo systemctl enable postfix || error_check "${ERROR}: postfix enabling failed, fix it and try again." 1
sudo systemctl start postfix || error_check "${ERROR}: postfix starting failed, fix it and try again." 1

echo ""
echo -e "${INFO}: Adding the GitLab package repository."
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
[[ $? -ne 0 ]] && error_check "{ERROR}: Addition of gitlab package repo failed." 1

echo ""
echo -e "${INFO}: Starting the installation of Gitlab-EE."
sudo yum install -y gitlab-ee
echo -e "${INFO}: Installation of Gitlab-EE completed."
echo ""
echo -e "${INFO}: Verifying the Gitlab URL"
url_stat=$(curl -s -o /dev/null -w "%{http_code}"  ${EXTERNAL_URL}/users/sign_in)
if [[ ${url_stat} -eq 200 ]]
then 
	echo -e "${INFO}: Hurray, installation successful."
else
	echo -e "${WARN}: Something wrong with URL, fix it and try in browser ${EXTERNAL_URL}"
fi

}

EXTERNAL_URL=${1}
if [[ -z ${1} ]]; then echo -en "${ERROR}: External URL is not mentioned for Gitlab installation, please specify:- "; read EXTERNAL_URL ; fi

counter=0

until [ $(echo ${EXTERNAL_URL} | grep ^http) ]
do
	echo -en "${ERROR}: The external mentioned is not a URL, it should start with http or https, try again:- "; read EXTERNAL_URL
	[[ $counter -gt 1 ]] && error_check "Exiting as the external URL is incorrect" 1
	((counter++))
done

pre_checks
case $cont in
      [yY]) main ;;
      [nN]) echo -e "${INFO}: We will work together when you are comfortable." ;;
      *) echo -e "${ERROR}: Invalid Input"
esac 

